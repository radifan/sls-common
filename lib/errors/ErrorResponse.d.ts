export default class ErrorResponse extends Error {
    body: string;
    statusCode: number;
    constructor(body: any, statusCode?: number, ...params: string[]);
}
