"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (function (data, statusCode) {
    if (data === void 0) { data = {}; }
    if (statusCode === void 0) { statusCode = 200; }
    return ({
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            data: data,
        }),
    });
});
