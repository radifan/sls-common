declare const _default: (data?: {}, statusCode?: number) => {
    statusCode: number;
    headers: {
        'Access-Control-Allow-Origin': string;
        'Access-Control-Allow-Credentials': boolean;
    };
    body: string;
};
export default _default;
