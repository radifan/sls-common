declare const _default: (message: any, statusCode?: number) => {
    statusCode: number;
    body: string;
};
export default _default;
