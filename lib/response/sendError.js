"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (function (message, statusCode) {
    if (statusCode === void 0) { statusCode = 500; }
    return ({
        statusCode: statusCode,
        body: JSON.stringify({
            message: message,
        }),
    });
});
;
