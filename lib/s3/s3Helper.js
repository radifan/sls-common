"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var aws_sdk_1 = require("aws-sdk");
var getInstance = function () { return new aws_sdk_1.S3({
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY,
    region: 'ap-southeast-1',
}); };
var generateS3Key = function (_a) {
    var folder = _a.folder, fileName = _a.fileName;
    var destinationFolder = folder ? folder + "/" : '';
    return "" + destinationFolder + fileName;
};
var uploadFile = function (_a) {
    var bucketName = _a.bucketName, buffer = _a.buffer, _b = _a.folder, folder = _b === void 0 ? '' : _b, fileName = _a.fileName;
    return new Promise(function (resolve, reject) {
        var s3 = getInstance();
        var params = {
            Bucket: bucketName,
            Body: buffer,
            Key: generateS3Key({
                folder: folder,
                fileName: fileName,
            }),
            ACL: 'public-read',
        };
        s3.upload(params, function (err, data) {
            if (err) {
                console.log(err);
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
};
exports.default = {
    uploadFile: uploadFile,
};
