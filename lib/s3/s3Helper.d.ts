declare const _default: {
    uploadFile: ({ bucketName, buffer, folder, fileName, }: {
        bucketName: any;
        buffer: any;
        folder?: string;
        fileName: any;
    }) => Promise<{}>;
};
export default _default;
