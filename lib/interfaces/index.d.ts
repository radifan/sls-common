export interface IEventPayload {
    method: string;
    query: any;
    body: string;
    headers: IEventHeaders;
}
export interface IEventResponse {
    statusCode: number;
    body: string;
}
interface IEventHeaders {
    Accept: string;
    Authorization: string;
    'accept-encoding': string;
    'cache-control': string;
    Connection: string;
    'content-length': string;
    'Content-Type': string;
    'Host': string;
    'Postman-Token': string;
    'User-Agent': string;
}
export {};
