interface MongoConfig {
    host: string;
    port: string;
    database: string;
    username: string;
    password: string;
}
export declare const configuration: {
    host: string;
    port: string;
    database: string;
    username: string;
    password: string;
};
declare const _default: () => Promise<MongoConfig>;
export default _default;
