export declare const getConnection: () => Promise<{}>;
export declare const closeConnection: () => Promise<{}>;
export declare const getInstance: () => any;
declare const _default: {
    getInstance: () => any;
};
export default _default;
