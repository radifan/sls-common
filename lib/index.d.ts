import handler from './handler';
import { IEventPayload, IEventResponse } from './interfaces';
import sendResponse from './response/sendResponse';
import sendError from './response/sendError';
import ErrorResponse from './errors/ErrorResponse';
import MongoContext from './mongo/connection';
import s3Helper from './s3/s3Helper';
export { handler, sendResponse, sendError, ErrorResponse, MongoContext, s3Helper, IEventPayload, IEventResponse, };
declare const _default: {
    handler: (fn: any) => Function;
    sendResponse: (data?: {}, statusCode?: number) => {
        statusCode: number;
        headers: {
            'Access-Control-Allow-Origin': string;
            'Access-Control-Allow-Credentials': boolean;
        };
        body: string;
    };
    sendError: (message: any, statusCode?: number) => {
        statusCode: number;
        body: string;
    };
    ErrorResponse: typeof ErrorResponse;
    MongoContext: {
        getInstance: () => any;
    };
    s3Helper: {
        uploadFile: ({ bucketName, buffer, folder, fileName, }: {
            bucketName: any;
            buffer: any;
            folder?: string;
            fileName: any;
        }) => Promise<{}>;
    };
};
export default _default;
