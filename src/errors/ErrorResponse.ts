export default class ErrorResponse extends Error {
  body: string;
  statusCode: number;
  constructor(body, statusCode = 500, ...params: string[]) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ErrorResponse);
    }

    this.body = body;
    this.statusCode = statusCode;
  }
}
