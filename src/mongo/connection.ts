import mongodb from 'mongodb';
import checkConfiguration, { configuration } from './checkConfiguration';

let mongoInstance = null;

export const getConnection = () => new Promise(async (resolve) => {
  try {
    const config = await checkConfiguration();
    const authentication = config.username && config.password ? `${config.username}:${config.password}@` : '';
    const connectionString = `mongodb://${authentication}${config.host}:${config.port}/admin`;

    mongoInstance = await mongodb.MongoClient.connect(connectionString, { useNewUrlParser: true });

    resolve(mongoInstance);
  } catch (err) {
    resolve(err);
  }
});

export const closeConnection = () => new Promise(async (resolve) => {
  if (mongoInstance) {
    const result = await mongoInstance.close();
    mongoInstance = null;

    resolve(result);
  }

  resolve();
});

export const getInstance = () => mongoInstance.db(configuration.database);

export default {
  getInstance,
};
