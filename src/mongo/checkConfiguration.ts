import { string, object } from 'yup';

interface MongoConfig {
  host: string,
  port: string,
  database: string,
  username: string,
  password: string,
}

const mongoConfigurationSchema = object().shape({
  host: string().required(),
  port: string().required(),
  database: string().required(),
  username: string(),
  password: string(),
});

export const configuration = {
  host: process.env.MONGODB_HOST || 'localhost',
  port: process.env.MONGODB_PORT || '27017',
  database: process.env.MONGODB_DATABASE || 'test',
  username: process.env.MONGODB_USERNAME,
  password: process.env.MONGODB_PASSWORD,
};

export default (): Promise<MongoConfig> => new Promise(async (resolve) => {
  const result = await mongoConfigurationSchema.validate(configuration);
  resolve(result);
});
