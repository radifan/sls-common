import { S3 } from 'aws-sdk';

const getInstance = () => new S3({
  accessKeyId: process.env.S3_ACCESS_KEY,
  secretAccessKey: process.env.S3_SECRET_KEY,
  region: 'ap-southeast-1',
});

const generateS3Key = ({
  folder,
  fileName,
}) => {
  const destinationFolder = folder ? `${folder}/` : '';
  return `${destinationFolder}${fileName}`;
}

const uploadFile = ({
  bucketName,
  buffer,
  folder = '',
  fileName,
}) => new Promise((resolve, reject) => {
  const s3 = getInstance();
  const params = {
    Bucket: bucketName,
    Body: buffer,
    Key: generateS3Key({
      folder,
      fileName,
    }),
    ACL: 'public-read',
  };

  s3.upload(params, (err, data) => {
    if (err) {
      console.log(err);
      reject(err);
    } else {
      resolve(data);
    }
  });
});

export default {
  uploadFile,
};
