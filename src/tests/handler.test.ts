import { handler, sendResponse } from '../index';

describe('handler test', () => {
  test('handler should return function', async () => {
    const event = 'event';
    const context = 'context';
    const fn = (event, context) => sendResponse({
      result: 'OK',
    }, 200);

    const handlerFunction = await handler(fn(event, context));

    expect(typeof handlerFunction).toBe("function");
  });

  test('handler function should return response', async () => {
    const fn = (event, context) => sendResponse({
      result: 'OK',
    }, 200);

    const handlerFunction = handler(fn);
    const response = await handlerFunction();

    expect(JSON.parse(response.body)).toEqual({
      data: {
        result: "OK",
      },
    });
    expect(response.statusCode).toEqual(200);
  });

  test('handler function should return error response', async () => {
    const fn = (event, context) => {
      throw new Error('Mock Error');
    }

    const handlerFunction = handler(fn);
    const response = await handlerFunction();

    expect(JSON.parse(response.body)).toEqual({
      message: 'Mock Error',
    });
    expect(response.statusCode).toEqual(500);
  });
})
