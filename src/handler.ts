import sendError from './response/sendError';
import { IEventResponse, IEventPayload } from './interfaces';
import { getConnection, closeConnection } from './mongo/connection';

export default (fn): Function => async (event: IEventPayload, context): Promise<IEventResponse> => {
  try {
    await getConnection();
    const response = await Promise.resolve(fn(event, context));
    await closeConnection();

    return response;
  } catch (error) {
    return sendError(error.body || error.message, error.statusCode);
  }
}
